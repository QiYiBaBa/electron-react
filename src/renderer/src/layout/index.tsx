import React, { useState, useMemo } from 'react'
import { Layout, Menu, theme, MenuProps, Spin } from 'antd'
import { Outlet, useNavigate, useLocation } from 'react-router-dom'
import { MinusOutlined, ExpandAltOutlined, CloseOutlined } from '@ant-design/icons'

import { useMenuList } from '@/router/useRoute'
import { getParentPaths } from '@/router/utils'

const { Header, Sider, Content, Footer } = Layout

const LayoutApp: React.FC = () => {
  const navigate = useNavigate()
  const { pathname } = useLocation()
  const { token } = theme.useToken()

  const [collapsed, setCollapsed] = useState(false)
  const [openKeys, setOpenKeys] = useState<string[]>([])
  const { menuList: menuItem } = useMenuList()

  const action = (type: string) => window.electronAPI.service({ type })
  const onClick: MenuProps['onClick'] = (e) => navigate(e.key)
  const onOpenChange: MenuProps['onOpenChange'] = (keys) => {
    const latestOpenKey = keys.find((key) => openKeys.indexOf(key) === -1)
    const rootSubmenuKeys = menuItem.filter((item) => item.children).map((item) => item.key)

    if (latestOpenKey && rootSubmenuKeys.indexOf(latestOpenKey!) === -1) {
      setOpenKeys(keys)
    } else {
      setOpenKeys(latestOpenKey ? [latestOpenKey] : [])
    }
  }

  const selectOpenKey = useMemo(() => {
    return [pathname]
  }, [pathname])

  React.useEffect(() => {
    if (!collapsed) {
      setOpenKeys(getParentPaths(pathname, menuItem))
    } else {
      setOpenKeys([])
    }
  }, [collapsed, pathname])

  return (
    <Layout style={{ minHeight: '100vh' }}>
      <Sider
        collapsible
        collapsed={collapsed}
        onCollapse={(value) => setCollapsed(value)}
        style={{ background: token.colorBgContainer, userSelect: 'none' }}
      >
        <div className="layout-logo-vertical" />
        <Menu
          mode="inline"
          openKeys={openKeys}
          onOpenChange={onOpenChange}
          selectedKeys={selectOpenKey}
          items={menuItem as MenuProps['items']}
          onClick={onClick}
          style={{ border: 'none' }}
        />
      </Sider>
      <Layout className="layout-main">
        <Header className="layout-header" style={{ background: token.colorBgContainer }}>
          <div className="layout-dragarea"></div>
          <div className="layout-icon" onClick={() => action('minimize')}>
            <MinusOutlined />
          </div>
          <div className="layout-icon" onClick={() => action('maximize')}>
            <ExpandAltOutlined />
          </div>
          <div className="layout-icon layout-icon_close" onClick={() => action('close')}>
            <CloseOutlined />
          </div>
        </Header>
        <Content style={{ margin: '0 16px' }}>
          <div style={{ margin: '16px 0' }}></div>
          <div
            style={{
              padding: 24,
              minHeight: 360,
              background: token.colorBgContainer,
              borderRadius: token.borderRadiusLG
            }}
          >
            <React.Suspense fallback={<Spin />}>
              <Outlet></Outlet>
            </React.Suspense>
          </div>
        </Content>
        <Footer style={{ textAlign: 'center' }}>
          Ant Design ©{new Date().getFullYear()} Created by Ant UED
        </Footer>
      </Layout>
    </Layout>
  )
}

export default LayoutApp
