import { useMemo } from 'react'
import { defaultRoute } from './modules'
import type { MenuItem, RouteList } from './route'

export const useRouteList = () => {
  function routeListToMenu(rtList: RouteList[], path?: React.Key): MenuItem[] {
    const menuList: MenuItem[] = []
    rtList.forEach((i: RouteList) => {
      const item = i
      if (item.meta.hideSidebar) return

      if (!item.alwaysShow && item.alwaysShow !== undefined && !item.element) {
        if (item.children && item.children[0]) {
          menuList.push(routeListToMenu([item.children[0]], item.path)[0])
          return
        }
      }

      let rtItem: MenuItem = {
        key: item.path,
        label: ''
      }
      if (path) rtItem.key = `${path}/${item.path}`

      rtItem = {
        ...rtItem,
        label: <span>{item.meta.label}</span>,
        icon: item.meta.icon
      }

      if (item.children && !item.element) {
        rtItem.children = routeListToMenu(item.children, rtItem.key)
      }

      menuList.push(rtItem)
    })

    return menuList
  }

  return { routeListToMenu }
}

export const useMenuList = () => {
  const { routeListToMenu } = useRouteList()

  const menuList = useMemo(() => {
    return routeListToMenu(defaultRoute)
  }, [])

  return { menuList }
}
