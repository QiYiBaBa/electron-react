import { lazy, createElement, Suspense, ComponentType } from 'react'
import { HomeOutlined, SettingOutlined, createFromIconfontCN } from '@ant-design/icons'
import { Navigate } from 'react-router-dom'
import { Spin } from 'antd'

import Layout from '@/layout'
import Authority from '@/components/Authority'

const IconFont = createFromIconfontCN({
  scriptUrl: ['https://at.alicdn.com/t/c/font_4431666_z0tu2sosyla.js']
})

const Setting = lazy(() => import('@/views/setting'))
const Login = lazy(() => import('@/views/login'))

const lazyElement = (element: () => Promise<{ default: ComponentType<any> }>) => (
  <Suspense fallback={<Spin />}>{createElement(lazy(element))}</Suspense>
)

export const defaultRoute = [
  {
    path: '/home',
    index: true,
    element: lazyElement(() => import('@/views/home')),
    meta: { label: '首页', icon: <HomeOutlined /> }
  },
  {
    path: '/electron',
    meta: { label: 'Electron', icon: <IconFont type="icon-electron" /> },
    children: [
      {
        index: true,
        path: 'index',
        element: lazyElement(() => import('@/views/electron/index')),
        meta: { label: '测试' }
      },
      {
        path: 'menu',
        element: lazyElement(() => import('@/views/electron/menu')),
        meta: { label: '菜单' }
      }
    ]
  },
  {
    path: '/React',
    meta: { label: 'React', icon: <IconFont type="icon-React" /> },
    children: [
      {
        index: true,
        path: 'Suspense',
        element: lazyElement(() => import('@/views/react/Suspense/index')),
        meta: { label: 'Suspense' }
      },
      {
        path: 'forwardRef',
        element: lazyElement(() => import('@/views/react/ForwardRef/index')),
        meta: { label: 'forwardRef' }
      }
    ]
  },
  {
    path: '/setting',
    element: <Setting />,
    meta: { label: '设置', icon: <SettingOutlined /> }
  }
]

export const whiteList = [
  {
    path: '*',
    element: <div>403</div>
  }
]

export const baseRouter = [
  {
    path: '/',
    element: (
      <Authority>
        <Layout />
      </Authority>
    ),
    children: [
      ...defaultRoute,
      ...whiteList,
      {
        path: '',
        element: <Navigate to={defaultRoute[0].path || ''} />
      }
    ]
  },
  {
    path: '/login',
    element: <Login />
  }
]
