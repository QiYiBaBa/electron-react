import { memo, useState } from 'react'
import { createBrowserRouter, RouterProvider } from 'react-router-dom'

import { baseRouter } from './modules'

const RouteView = memo(() => {
  const [route, setRoute] = useState(baseRouter)

  const routeElement = createBrowserRouter(route)

  return <RouterProvider router={routeElement} />
})

export default RouteView
