import type { MenuItem, RouteList } from '@/router/route'
import type { AsyncRouteType } from '@/store/modules/route'
import { defaultRoute } from './modules'
import { cloneDeep } from 'lodash-es'

import { setStoreAsyncRouter } from '@/store/modules/route'
import { getRouteApi } from '@/api/index'
import store from '@/store'

export async function initAsyncRoute(power: string) {
  store.dispatch(setStoreAsyncRouter([]))
  const res = await getRouteApi({ name: power })

  if (res.data.length) {
    console.log(res.data)
    store.dispatch(setStoreAsyncRouter(res.data))
  }
  return ''
}

export function handlePowerRoute(
  dataRouter: AsyncRouteType[],
  routerList: RouteList[] = defaultRoute
) {
  const newRouteList: RouteList[] = []
  routerList.forEach((i) => {
    const item = cloneDeep(i)
    if (!item.meta.whiteList) {
      const rItem = dataRouter.find((r) => r.id === item.id)
      if (rItem) {
        if (rItem.children && rItem.children.length && item.children && item.children.length) {
          const children = handlePowerRoute(rItem.children, item.children)
          item.children = children
          if (children) newRouteList.push(item)
        } else {
          newRouteList.push(item)
        }
      }
    } else {
      console.log(item)
      newRouteList.push(item)
    }
  })
  return newRouteList
}

// 通过path获取父级路径
export function getParentPaths(routePath: string, routes: MenuItem[]): string[] {
  // 深度遍历查找
  function dfs(routes: MenuItem[], key: string, parents: string[]) {
    for (let i = 0; i < routes.length; i++) {
      const item = routes[i]
      // 找到key则返回父级key
      if (item.key === key) return [item.key]
      // children不存在或为空则不递归
      if (!item.children || !item.children.length) continue
      // 往下查找时将当前key入栈
      parents.push(item.key as string)

      if (dfs(item.children, key, parents).length) return parents
      // 深度遍历查找未找到时当前path 出栈
      parents.pop()
    }
    // 未找到时返回空数组
    return []
  }
  return dfs(routes, routePath, [])
}
