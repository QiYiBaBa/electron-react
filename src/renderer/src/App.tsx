import React from 'react'
import { ConfigProvider, theme, Spin } from 'antd'

import RouteView from '@/router'
import { initAsyncRoute } from '@/router/utils'
import store from '@/store'
import { useAppSelector } from '@/store/hooks'
import { setAppConfiguration } from '@/store/modules/app'

function App(): JSX.Element {
  const { userInfo } = useAppSelector((state) => state.user)
  const asyncRouter = useAppSelector((state) => state.route.asyncRouter)
  const themeSource = useAppSelector((state) => state.app.nativeTheme.themeSource)

  React.useEffect(() => {
    if (!asyncRouter.length && userInfo) {
      initAsyncRoute(userInfo.power)
    }

    window.electronAPI.getConfiguration((value) => {
      store.dispatch(setAppConfiguration(value))
    })
  }, [])

  const loading = React.useMemo(() => {
    if (!asyncRouter.length && userInfo) {
      return true
    }
    return false
  }, [asyncRouter])

  return (
    <>
      {loading ? (
        <div
          style={{
            textAlign: 'center',
            borderRadius: '4px',
            height: '100%',
            display: 'flex',
            alignItems: 'center',
            justifyContent: 'center'
          }}
        >
          <Spin />
        </div>
      ) : (
        <ConfigProvider
          theme={{
            algorithm: themeSource === 'dark' ? theme.darkAlgorithm : theme.defaultAlgorithm
          }}
        >
          <React.Suspense fallback={<Spin />}>
            <RouteView />
          </React.Suspense>
        </ConfigProvider>
      )}
    </>
  )
}

export default App
