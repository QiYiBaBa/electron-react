import type { ModalFuncProps } from 'antd'
import { message, Modal } from 'antd'

function createElMessageBox(messageg: string, title: string, options: ModalFuncProps) {
  Modal.error({ title, content: messageg, ...options })
}

export function createErrorModal(msg: string) {
  createElMessageBox(msg, '错误提示', { centered: true })
}

export function createErrorMsg(msg: string) {
  message.error(msg)
}

export function useMessage() {
  return {
    createErrorModal,
    createErrorMsg
  }
}
