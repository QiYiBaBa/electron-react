import { merge } from 'lodash-es'
import type { PayloadAction } from '@reduxjs/toolkit'
import { createSlice } from '@reduxjs/toolkit'

export type LocaleType = 'zh-CN' | 'en-US'
export type ThemeMode = 'dark' | 'light'
export type SidebarMode = 'vertical' | 'horizontal' | 'blend'

export interface AppConfigMode {
  version: string
  pythonServer: false
  browserWindow: object
  nativeTheme: { themeSource: ThemeMode; shouldUseDarkColors: boolean }
}

const initialState: AppConfigMode = {
  version: '1.0.0',
  pythonServer: false,
  browserWindow: {
    title: 'levi app',
    height: 810,
    width: 1440,
    show: false,
    resizable: false,
    transparent: true,
    autoHideMenuBar: false,
    frame: true,
    hasShadow: true,
    titleBarStyle: 'hidden'
  },
  nativeTheme: { themeSource: 'dark', shouldUseDarkColors: true }
}

export const appSlice = createSlice({
  name: 'appConfig',
  initialState,
  reducers: {
    setAppConfiguration: (state, action: PayloadAction<object>) => {
      state = merge(state, action.payload)
    }
  }
})

// 每个 case reducer 函数会生成对应的 Action creators
export const { setAppConfiguration } = appSlice.actions

export default appSlice.reducer
