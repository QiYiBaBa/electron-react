import store from '@/store'
import React, { useState, useEffect } from 'react'
import { useAppSelector } from '@/store/hooks'
import { setAppConfiguration } from '@/store/modules/app'
import { Radio, Descriptions, Button } from 'antd'
import type { DescriptionsProps, RadioChangeEvent } from 'antd'

const Setting: React.FC = () => {
  const app = useAppSelector((state) => state.app)
  const [configuration, setConfiguration] = useState(app)

  const themeOptions = [
    { label: '系统', value: 'system' },
    { label: '亮色', value: 'light' },
    { label: '暗色', value: 'dark' }
  ]

  const onThemeChange = ({ target: { value } }: RadioChangeEvent) => {
    store.dispatch(setAppConfiguration({ nativeTheme: { themeSource: value } }))
  }

  useEffect(() => {
    setConfiguration(app)
  }, [app])

  const items: DescriptionsProps['items'] = [
    {
      key: '1',
      label: '主题',
      children: (
        <Radio.Group
          options={themeOptions}
          onChange={onThemeChange}
          value={configuration.nativeTheme.themeSource}
          optionType="button"
          buttonStyle="solid"
        />
      )
    },
    {
      key: '2',
      label: '更新',
      children: <Button>检测更新</Button>
    }
  ]

  return <Descriptions size={'small'} column={1} title="设置" bordered items={items} />
}

export default Setting
