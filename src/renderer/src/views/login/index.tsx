import { Button, Layout, theme } from 'antd'
import React, { memo, useEffect, useState } from 'react'
import { useNavigate } from 'react-router-dom'
import { MinusOutlined, ExpandAltOutlined, CloseOutlined, ChromeOutlined } from '@ant-design/icons'

import { getUserInfo } from '@/api/index'
import { initAsyncRoute } from '@/router/utils'
import { setUserInfo } from '@/store/modules/user'
import { useAppDispatch, useAppSelector } from '@/store/hooks'

const { Header, Content } = Layout

const Login: React.FC = memo(() => {
  const dispatch = useAppDispatch()
  const navigate = useNavigate()
  const userStore = useAppSelector((state) => state.user)

  const [loading, setLoading] = useState<boolean>(false)

  const {
    token: { colorBgContainer }
  } = theme.useToken()
  const action = (type: string) => window.electronAPI.service({ type })
  const login = async () => {
    setLoading(true)

    const res = await getUserInfo('admin', 'admin123')
    setTimeout(async () => {
      if (res.code === 1) {
        await initAsyncRoute(res.data.power)
        dispatch(setUserInfo(res.data))
      }
      setLoading(false)
    }, 1000)
  }

  useEffect(() => {
    if (userStore.power) {
      navigate('/home')
    }
  }, [userStore])

  return (
    <Layout className="layout-main">
      <Header className="layout-header" style={{ background: colorBgContainer }}>
        <div className="layout-dragarea">
          <span className="layout-icon">
            <ChromeOutlined />
          </span>
          levi
        </div>
        <div className="layout-icon" onClick={() => action('minimize')}>
          <MinusOutlined />
        </div>
        <div className="layout-icon" onClick={() => action('maximize')}>
          <ExpandAltOutlined />
        </div>
        <div className="layout-icon layout-icon_close" onClick={() => action('close')}>
          <CloseOutlined />
        </div>
      </Header>
      <Content className="layout-content" style={{ background: colorBgContainer }}>
        <Button loading={loading} onClick={() => login()}>
          登录
        </Button>
      </Content>
    </Layout>
  )
})

export default Login
