import React from 'react'
import ArtistPage from './ArtistPage'
import SearchResults from './SearchResults'

const CardCom = () => {
  const [loading, setLoading] = React.useState(false)
  const [data, setData] = React.useState({
    title: '',
    desc: ''
  })

  React.useEffect(() => {
    if (!loading) {
      setLoading(true)
      new Promise((resolve) => {
        setTimeout(() => {
          resolve({
            title: 'Lorem ipsum dolor sit amet',
            desc: 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Dolore perferendis'
          })
        }, 1000)
      }).then((res: any) => {
        setData(res)
        setLoading(false)
      })
    }
  }, [])

  if (loading) {
    throw Promise.resolve(null) // 这里抛出一个 Promise
  } else {
    return (
      <div>
        <ul>
          <li>{data.title}</li>
          <li>{data.desc}</li>
        </ul>
      </div>
    )
  }
}

const Index = () => {
  const [query, setQuery] = React.useState('')
  const deferredQuery = React.useDeferredValue(query)
  const isStale = query !== deferredQuery

  return (
    <div>
      <div>
        <h1>CardCom</h1>
        <React.Suspense fallback={<h1>CardCom 正在加载....</h1>}>
          <CardCom />
        </React.Suspense>
      </div>

      <ArtistPage
        artist={{
          id: 'the-beatles',
          name: 'The Beatles'
        }}
      />
      <div style={{ height: '32px' }}></div>
      <label>
        Search albums:
        <input value={query} onChange={(e) => setQuery(e.target.value)} />
      </label>
      <React.Suspense fallback={<h2>Loading...</h2>}>
        <div style={{ opacity: isStale ? 0.5 : 1 }}>
          <SearchResults query={deferredQuery} />
        </div>
      </React.Suspense>
    </div>
  )
}

export default Index
