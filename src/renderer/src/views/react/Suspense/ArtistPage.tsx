import { Suspense } from 'react'
import { fetchData } from './data.js'
import classes from './ArtistPage.module.css'

export default function ArtistPage({ artist }) {
  return (
    <>
      <h1>{artist.name}</h1>
      <Suspense fallback={<BigSpinner />}>
        <Biography artistId={artist.id} />
        <Suspense fallback={<AlbumsGlimmer />}>
          <Panel>
            <Albums artistId={artist.id} />
          </Panel>
        </Suspense>
      </Suspense>
    </>
  )
}

function BigSpinner() {
  return <h2>🌀 Loading...</h2>
}

function AlbumsGlimmer() {
  return (
    <div className={classes['glimmer-panel']}>
      <div className={classes['glimmer-line']} />
      <div className={classes['glimmer-line']} />
      <div className={classes['glimmer-line']} />
    </div>
  )
}

// 注意：这个组件使用了一个实验性的 API
// 该 API 并未在 React 的稳定版本中可用

// 对于一个现实的例子，你可以尝试一个
// 与 Suspense 集成的框架，例如 Relay 或 Next.js。

function Albums({ artistId }) {
  const albums = use(fetchData(`/${artistId}/albums`))
  return (
    <ul>
      {albums.map((album) => (
        <li key={album.id}>
          {album.title} ({album.year})
        </li>
      ))}
    </ul>
  )
}

function Biography({ artistId }) {
  const bio = use(fetchData(`/${artistId}/bio`))
  return (
    <section>
      <p className={classes.bio}>{bio}</p>
    </section>
  )
}

function Panel({ children }) {
  return <section className={classes.panel}>{children}</section>
}

// 这是一个解决 bug 的临时方案，以便让演示运行起来。
// TODO：当 bug 修复后，用真正的实现替换。
export function use(promise) {
  if (promise.status === 'fulfilled') {
    return promise.value
  } else if (promise.status === 'rejected') {
    throw promise.reason
  } else if (promise.status === 'pending') {
    throw promise
  } else {
    promise.status = 'pending'
    promise.then(
      (result) => {
        promise.status = 'fulfilled'
        promise.value = result
      },
      (reason) => {
        promise.status = 'rejected'
        promise.reason = reason
      }
    )
    throw promise
  }
}
