import React, { ForwardedRef } from 'react'

interface MyInputProps {
  label?: string
  src?: string
  type?: string
  width?: string
  ref: ForwardedRef<any>
}

const MyInput = React.forwardRef(function MyInput(
  props: MyInputProps,
  forwardedRef: ForwardedRef<any>
) {
  const { label, ...otherProps } = props
  return (
    <label>
      {label}
      <input {...otherProps} ref={forwardedRef} />
    </label>
  )
})

const VideoPlayer = React.forwardRef(function VideoPlayer(
  { src, type, width }: MyInputProps,
  forwardedRef: ForwardedRef<any>
) {
  return (
    <video width={width} ref={forwardedRef}>
      <source src={src} type={type} />
    </video>
  )
})

const ForwardRef: React.FC = () => {
  const inputRef = React.useRef<HTMLInputElement>(null)
  const vedioRef = React.useRef<HTMLInputElement>(null)

  function handleClick() {
    inputRef.current?.focus()
  }

  return (
    <form>
      <MyInput label="Enter your name:" ref={inputRef} />
      <button type="button" onClick={handleClick}>
        编辑
      </button>

      <br />
      <br />
      <br />
      <br />

      <button onClick={() => vedioRef.current?.play()}>Play</button>
      <button onClick={() => vedioRef.current?.pause()}>Pause</button>
      <br />
      <VideoPlayer
        ref={vedioRef}
        src="https://interactive-examples.mdn.mozilla.net/media/cc0-videos/flower.mp4"
        type="video/mp4"
        width="250"
      />
    </form>
  )
}

export default ForwardRef
