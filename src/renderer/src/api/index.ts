import type { AsyncRouteType } from '@/store/modules/route'
import { deffHttp } from '@/utils/axios'

interface Param {
  name: string
}

interface UseInfoType {
  name: string
  userid: string
  email: string
  signature: string
  introduction: string
  title: string
  token: string
  power: 'test' | 'admin'
}

export const getRouteApi = (data: Param) =>
  deffHttp.post<AsyncRouteType[]>({ url: '/mock_api/getRoute', data })

export const getUserInfo = (user: string, pwd: string) =>
  deffHttp.post<UseInfoType>(
    {
      url: '/mock_api/login',
      data: { username: user, password: pwd }
    },
    { errorMessageMode: 'modal', withToken: false }
  )
