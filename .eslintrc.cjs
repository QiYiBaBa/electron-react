module.exports = {
  root: true,
  env: {
    browser: true,
    commonjs: true,
    es6: true,
    node: true
  },
  extends: [
    'eslint:recommended',
    'plugin:react/recommended',
    'plugin:react/jsx-runtime',
    '@electron-toolkit/eslint-config-ts/recommended',
    '@electron-toolkit/eslint-config-prettier'
  ],
  extends: [
    // 'eslint:recommended'
    // 'plugin:@typescript-eslint/recommended',
    // 'plugin:react-hooks/recommended',
    // 'plugin:react/recommended',
    // 'plugin:react/jsx-runtime'
    // '@electron-toolkit/eslint-config-ts/recommended',
    // '@electron-toolkit/eslint-config-prettier'
  ],
  ignorePatterns: ['dist', '.eslintrc.cjs'],
  parser: '@typescript-eslint/parser',
  // plugins: ['react-refresh'],
  rules: {
    // 'react-refresh/only-export-components': [
    //   'warn',
    //   {
    //     allowConstantExport: true,
    //     allowExportNames: ['meta', 'links', 'headers', 'loader', 'action']
    //   }
    // ],
    '@typescript-eslint/ban-ts-comment': 'off',
    'no-unused-vars': 'off',
    '@typescript-eslint/no-unused-vars': 'off'
  }
}
