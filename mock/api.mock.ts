import { defineMock } from 'vite-plugin-mock-dev-server'

const userInfo = {
  name: '爱喝蜂蜜绿的小斯斯',
  userid: '00000001',
  email: '1531733886@qq.com',
  signature: '甜甜的蜂蜜，甘甜的绿茶，蜂蜜中和了绿茶的苦涩保留了绿茶回甘，绝妙啊',
  introduction: '微笑着，努力着，欣赏着',
  title: '小斯斯',
  token: '',
  power: 'admin'
}

const userInfo2 = {
  name: 'test',
  userid: '00000002',
  email: '12312311223@qq.com',
  signature: '小啊小啊浪',
  introduction: '一个只会喝蜂蜜绿的小前端',
  title: '咪咪咪',
  token: '',
  power: 'test'
}

const power = [
  {
    path: '/home',
    id: 'Home'
  },
  {
    path: '/nested',
    id: 'Nested',
    children: [
      {
        path: 'menu1',
        id: 'Menu1',
        children: [
          {
            path: 'menu1-1',
            id: 'Menu1-1'
          },
          {
            path: 'menu1-2',
            id: 'Menu1-2'
          }
        ]
      }
    ]
  },
  {
    path: '/react-router',
    id: 'ReactRouter',
    children: [
      {
        path: 'tutorial',
        id: 'Rutorial',
        children: [
          {
            path: 'index',
            id: 'RutorialIndex'
          },
          {
            path: 'contacts/:contactId',
            id: 'RutorialContact'
          },
          {
            id: 'RutorialEdit',
            path: 'contacts/:contactId/edit'
          },
          {
            id: 'RutorialDestroy',
            path: 'contacts/:contactId/destroy'
          }
        ]
      }
    ]
  }
]

// const adminRoute = [
//   {
//     path: '/power',
//     id: 'Power',
//     children: [
//       {
//         path: 'permissions',
//         id: 'Permissions',
//       },
//       {
//         path: 'test-permissions-a',
//         id: 'TestPermissionsA',
//       },
//     ],
//   },
// ];

// const testRoute = [
//   {
//     path: '/power',
//     id: 'Power',
//     children: [
//       {
//         path: 'permissions',
//         id: 'Permissions',
//       },
//       {
//         path: 'test-permissions-b',
//         id: 'TestPermissionsB',
//       },
//     ],
//   },
// ];

export default defineMock([
  {
    url: '/mock_api/getUserInfo',
    method: 'GET',
    response(req, res, next) {
      res.end(JSON.stringify(userInfo))
    }
  },
  {
    url: '/mock_api/login',
    method: 'POST',
    response: (req, res, next) => {
      const { query, body, params, headers } = req
      const { username, password } = body

      if (username == 'admin' && password == 'admin123') {
        userInfo.token = genID(16)
        res.end(
          JSON.stringify({
            data: userInfo,
            code: 1,
            message: 'ok'
          })
        )
      } else if (username == 'test' && password == 'test123') {
        userInfo2.token = genID(16)
        res.end(
          JSON.stringify({
            data: userInfo2,
            code: 1,
            message: 'ok'
          })
        )
      } else {
        res.end({
          data: null,
          code: -1,
          message: '账号密码错误'
        })
      }
    }
  },
  {
    url: '/mock_api/getRoute',
    method: 'POST',
    response: (req, res, next) => {
      const { query, body, params, headers } = req
      const { name } = body
      if (name == 'admin') {
        res.end(
          JSON.stringify({
            // data: [...power, ...adminRoute],
            data: [...power],
            code: 1,
            message: 'ok'
          })
        )
      } else if (name == 'test') {
        res.end(
          JSON.stringify({
            // data: [...power, ...testRoute],
            data: [...power],
            code: 1,
            message: 'ok'
          })
        )
      } else {
        res.end(
          JSON.stringify({
            data: [],
            code: -1,
            message: '账号错误'
          })
        )
      }
    }
  }
])

function genID(length: number) {
  return Number(Math.random().toString().substr(3, length) + Date.now()).toString(36)
}
